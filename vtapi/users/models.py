# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


class UserManger(BaseUserManager):

    def create_user(self, phone_number, password=None):

        if not phone_number:
            raise ValueError(_("Users must have phone number. Without phone number users will be directed to xyz."))

        user = self.model(phone_number=phone_number)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone_number, password=None):

        user = self.create_user(phone_number=phone_number)
        user.set_password(password)
        user.is_admin = True
        user.is_moderator = True
        user.save(using=self._db)
        return user

    def create_moderator(self, phone_number, password=None):

        user = self.create_user(phone_number=phone_number)
        user.is_moderator = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):

    phone_number_regex = RegexValidator(
        regex=r'^\+?1?\d{9,12}$',
        message=_("Phone number be entered in format: +9989712345678")
    )

    phone_number = models.CharField(
        verbose_name=_("Phone number"),
        max_length=13, 
        primary_key=True,
        validators=[phone_number_regex]
    )

    nickname = models.CharField(
        verbose_name=_("User nickname"),
        max_length=16, 
        unique=True,
        null=False, 
        blank=False 
    )

    auth_token = models.CharField(
        verbose_name=_("Authentication token"),
        max_length=32,
        unique=True,
        null=True,
        blank=True
    )

    first_name = models.CharField(
        verbose_name=_("First name"),
        max_length=16, 
        null=False, 
        blank=False
    )

    last_name = models.CharField(
        verbose_name=_("Last name"),
        max_length=16, 
        null=False, 
        blank=False
    )

    is_admin = models.BooleanField(
        verbose_name=_("Administrator"),
        default=False, 
        null=False, 
        blank=False
    )

    is_moderator = models.BooleanField(
        verbose_name=_("Moderator"),
        default=False, 
        null=False, 
        blank=False
    )

    is_member = models.BooleanField(
        verbose_name=_("Member"),
        default=False, 
        null=False, 
        blank=False
    )

    verified = models.BooleanField(
        verbose_name=_("Verified"),
        default=False, 
        null=False, 
        blank=False
    )

    joined = models.DateTimeField(
        verbose_name=_("Joined date"),
        auto_now=True,
        null=False,
        blank=False
    )

    last_seen = models.DateTimeField(
        verbose_name=_("Last seen"),
        auto_now_add=True,
        null=False,
        blank=False
    )

    is_staff = True

    objects = UserManger()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    def __unicode__(self):
        return unicode(self.phone_number)

    class Meta:
        db_table = "users"
        ordering = ('nickname', '-joined')
